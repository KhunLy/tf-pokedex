import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MediatorService {

  
  private _selectedUrl : string;
  public get selectedUrl() : string {
    return this._selectedUrl;
  }
  public set selectedUrl(v : string) {
    this._selectedUrl = v;
    if(v != null){
      this.httpClient
        .get<PokemonDetailsRequest>(this.selectedUrl)
        .subscribe(json => {
        this.selectedPokemon.next(json);
      });
    }
  }

  public selectedPokemon: Subject<PokemonDetailsRequest>
    = new Subject<PokemonDetailsRequest>();
  

  constructor(private httpClient: HttpClient) { }
}
