import { Component, OnInit } from '@angular/core';
import { MediatorService } from 'src/app/services/mediator.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  model: PokemonDetailsRequest;

  constructor(private mediator: MediatorService) { }

  ngOnInit() {
    this.mediator.selectedPokemon.subscribe(
      data => { this.model = data; }
    );
  }

}
