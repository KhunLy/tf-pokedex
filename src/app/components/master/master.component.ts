import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MediatorService } from 'src/app/services/mediator.service';

@Component({
  selector: 'app-master',
  templateUrl: './master.component.html',
  styleUrls: ['./master.component.scss']
})
export class MasterComponent implements OnInit {

  public model: PokemonRequest;

  constructor(
    private httpClient: HttpClient,
    private mediator: MediatorService
  ) { }

  ngOnInit() {
    //faire une requete vers l'api pokemon
    this.httpClient
      .get<PokemonRequest>('https://pokeapi.co/api/v2/pokemon')
      .subscribe(json => {
        this.model = json;
      });
  }

  prev() {
    this.httpClient
      .get<PokemonRequest>(this.model.previous)
      .subscribe(json => {
        this.model = json;
      });
  }

  next() {
    this.httpClient
    .get<PokemonRequest>(this.model.next)
    .subscribe(json => {
      this.model = json;
    });
  }

  select(url: string){
    this.mediator.selectedUrl = url;
  }

}
