import { Injectable } from '@angular/core';
import { HttpInterceptor } from '@angular/common/http';
import { finalize } from 'rxjs/operators';
import { LoaderService } from '../services/loader.service';

@Injectable()
export class LoaderInterceptorService implements HttpInterceptor{

  intercept(req: import("@angular/common/http").HttpRequest<any>, next: import("@angular/common/http").HttpHandler): import("rxjs").Observable<import("@angular/common/http").HttpEvent<any>> {
    // afficher mon loader
    this.loaderService.isVisible = true;
    return next.handle(req).pipe(finalize(() => {
      // ne plus afficher le loader
      this.loaderService.isVisible = false;
    }));
    
  }

  constructor(private loaderService: LoaderService) { }
}
